﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] tab_wej = new int[10,10] {  {0,1,1,1,0,0,1,0,0,0},
                                          {0,0,1,1,0,1,0,1,0,0}, 
                                          {0,1,0,0,0,0,0,0,0,0}, 
                                          {0,1,0,0,0,0,0,0,0,0}, 
                                          {0,1,0,1,0,1,0,1,0,0}, 
                                          {0,1,1,0,1,1,0,0,1,0}, 
                                          {0,0,0,0,0,0,0,1,0,0},
                                          {0,1,1,0,1,0,1,0,1,0}, 
                                          {1,1,1,0,1,0,1,0,1,0}, 
                                          {1,0,1,0,1,0,1,1,0,1} };
            Game_of_life gra = new Game_of_life(tab_wej);
            gra.graj();
        }
    }
    public class Game_of_life
    {
        
        private int[,] komorki;
        public Game_of_life(int [,] tab ) 
        {
            komorki = new int[tab.GetLength(0),tab.GetLength(1)];
            Array.Copy(tab, komorki, tab.Length);

        }
        public int zlicz_sasiadow(int x, int y)
        {
            int licznik = 0;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (i == 0 && j == 0) ;
                    else if (x + i < 0 || y + j < 0) ;
                    else if (x + i > 9 || y + j > 9) ;
                    else if (komorki[x + i, y + j] == 1)
                        licznik++;
                }
            }
            return licznik;
        }
        public void graj()
        {
            int[,] temp = new int[komorki.GetLength(0), komorki.GetLength(1)];
            while (true)
            {
                for (int i = 0; i < komorki.GetLength(0); i++)
                {
                    for (int j = 0; j < komorki.GetLength(1); j++)
                    {
                        int liczba_sasiadow = zlicz_sasiadow(i, j);
                        if (komorki[i, j] == 1)
                        { //czy zywa
                            if (liczba_sasiadow == 2 || liczba_sasiadow == 3) temp[i, j] = 1;
                            else temp[i, j] = 0;
                        }
                        //jesli martwa
                        else if (liczba_sasiadow == 3) temp[i, j] = 1;
                    }

                }
               
                Array.Copy(temp, komorki, temp.Length);
                for (int a = 0; a < 10; a++)
                    for (int b = 0; b < 10; b++)
                    {
                        Console.SetCursorPosition(a, b);
                        Console.Write(komorki[a, b]);

                    }

                Console.ReadKey();
            }
        }

    }
}
