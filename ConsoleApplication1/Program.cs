﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {

        static int[,] komorki = new int[10,10] {  {0,1,1,1,0,0,1,0,0,0},
                                               {0,0,1,1,0,1,0,1,0,0}, 
                                               {0,1,0,0,0,0,0,0,0,0}, 
                                               {0,1,0,0,0,0,0,0,0,0}, 
                                               {0,1,0,1,0,1,0,1,0,0}, 
                                               {0,1,1,0,1,1,0,0,1,0}, 
                                               {0,0,0,0,0,0,0,1,0,0},
                                               {0,1,1,0,1,0,1,0,1,0}, 
                                               {1,1,1,0,1,0,1,0,1,0}, 
                                               {1,0,1,0,1,0,1,1,0,1} };
        static int[,] temp = new int[10,10];


        static void Main(string[] args)
        {
            while(true){
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    int liczba_sasiadow=zlicz_sasiadow(i, j);
                    if(komorki[i,j] == 1){ //czy zywa
                        if(liczba_sasiadow ==2 || liczba_sasiadow == 3) temp[i,j] = 1;
                        else temp[i,j] = 0;
                    }
                        //jesli martwa
                    else if (liczba_sasiadow ==3) temp[i,j] = 1;
                }

            }
            /*for(int l = 0; l<10; l++)
                for(int k = 0; k<10; k++)
                    komorki[l,k] = temp[l,k];
            */
            Array.Copy(temp, komorki, temp.Length);
            for(int a = 0; a<10; a++)
                for(int b = 0; b<10; b++){
                    Console.SetCursorPosition(a,b);
                    Console.Write(komorki[a,b]);
                    
                }
                    
                Console.ReadKey();
            }
        }

        static int zlicz_sasiadow(int x, int y) {
            int licznik=0;
            for(int i=-1; i<=1; i++) {
                for(int j=-1; j<=1; j++) {
                    if (i == 0 && j == 0) ;
                    else if (x + i < 0 || y + j < 0) ;
                    else if (x + i > 9 || y + j > 9) ;
                    else if (komorki[x + i, y + j] == 1)
                        licznik++;
                    }
                }
            return licznik;
        }

    }
}
